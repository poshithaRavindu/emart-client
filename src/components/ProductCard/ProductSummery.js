import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
import { AddProductToCart } from "../../pages/user/services/user-service";
import { getFormattedPriceRange } from "../../currnecyHandler";
import Cookies from "js-cookie";
import Box from "@material-ui/core/Box";
import cartContext from "../../services/cartContext";


const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    height: "calc(100% - 0px)",
    justifyContent: "space-between",
    display: "flex",
    flexDirection: "column"
  },
  media: {
    height: 0,
    paddingTop: "56.25%"
  },
  actions: {
    marginBottom: 0,
    paddingBottom: 9
  }
});

// Container components handle all state changes. They are interested in data and how it changes
export default function ProductSummery(props) {
  const classes = useStyles();
  const [cartProductCount, setcartProductCount] = useContext(cartContext);

  const handleClick = (message, type) => {
    props.handleClick(message, type);
  };


  const handleAddProductoCart = async () => {
    if (!Cookies.get("userID")) {
      handleClick("Please loging to add the item to cart", "error");
    } else {
      let newItem = {
        userId: Cookies.get("userID"),
        productId: props.product._id,
        quantity: 1
      };

      //temporilly commented
      await AddProductToCart(newItem);

      setcartProductCount(cartProductCount + 1);

      handleClick(`${props.product.title} is added to your cart`, "success");
  }
}
  

  return (
    <React.Fragment>
      <Link
        to={`/product/${props.product._id}`}
        style={{ textDecoration: "none" }}
      >
        <Card className={classes.root}>
          <CardMedia
            className={classes.media}
            image={props.product.imageUrl}
            title="Contemplative Reptile"
          />

          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {props.product.title}
            </Typography>
            <Box
              component="div"
              textOverflow="ellipsis"
              overflow="hidden"
              bgcolor="background.paper"
              style={{ width: 200, whiteSpace: "nowrap" }}
            >
              {props.product.description}
            </Box>
            <Typography variant="h6" color="secondary" component="p">
              {getFormattedPriceRange(props.product.price)
                ? getFormattedPriceRange(props.product.price)
                : `Rs.${props.product.price}.00`}
            </Typography>
          </CardContent>
          <CardActions className={classes.actions}>
            <Button
              component={Link}
              vsize="small"
              variant="contained"
              color="primary"
              onClick={handleAddProductoCart}
            >
              Add to Cart
            </Button>
          </CardActions>
        </Card>
      </Link>
    </React.Fragment>
  );
}
