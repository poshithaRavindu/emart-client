import React, { Suspense, useContext } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Badge from "@material-ui/core/Badge";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { useStyles } from "./HeaderStyle";
import SearchBar from "../../pages/home/components/SearchBar";
import { Link } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import Cookies from "js-cookie";
import cartContext from "../../services/cartContext";
const CartDialog = React.lazy(() =>
  import("../../pages/user/components/CartDialog")
);

function Header() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [adminPanel, setAdminPanel] = React.useState(false);
  const [cartProductCount, setcartProductCount] = useContext(cartContext);
  const isMenuOpen = Boolean(anchorEl);

  const [open, setOpen] = React.useState(false);

  const handleClickOpenCart = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
    if (Cookies.get("userType") === "Admin") {
      setAdminPanel(true);
    }
  };

  const logOutBtnClick = () => {
    try {
      Cookies.remove("userID");
      Cookies.remove("userType");
      Cookies.remove("token");
      Cookies.seremovet("userName");
    } catch (error) { }
    window.location.href = "/login";
  };

  const logInBtnClick = () => {
    window.location.href = "/login";
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem component={Link} to="/user/" onClick={handleMenuClose}>
        Profile
      </MenuItem>
      <MenuItem component={Link} to="/user/myorders" onClick={handleMenuClose}>
        My Orders
      </MenuItem>
      <MenuItem
        component={Link}
        to="/admin"
        onClick={handleMenuClose}
        style={{ display: adminPanel ? "block" : "none" }}
      >
        Admin Panel
      </MenuItem>
      <MenuItem onClick={logOutBtnClick}>Logout</MenuItem>
    </Menu>
  );

  const loggedMenuWithUser = (
    <React.Fragment>
      <div className={classes.grow} />
      <Typography variant="subtitle2" className={classes.username}>
        <b>Hi! {Cookies.get("userName")}</b>
      </Typography>
      <div className={classes.sectionDesktop}>
        <IconButton color="inherit" onClick={handleClickOpenCart}>
          <Badge
            id="cart-icon"
            badgeContent={cartProductCount}
            color="secondary"
          >
            <ShoppingCartIcon />
          </Badge>
        </IconButton>
        <IconButton
          edge="end"
          aria-label="account of current user"
          aria-controls={menuId}
          aria-haspopup="true"
          onClick={handleProfileMenuOpen}
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
      </div>
    </React.Fragment>
  );

  const unLoggedMenu = (
    <React.Fragment>
      <div className={classes.grow} />
      <div className={classes.sectionDesktop}>
        <Button color="inherit" onClick={logInBtnClick}>
          Login
        </Button>
      </div>
    </React.Fragment>
  );

  const loadRightMenu = !Cookies.get("userID")
    ? unLoggedMenu
    : loggedMenuWithUser;

  return (
    <div className={classes.grow}>
      <Suspense fallback={<CircularProgress />}>
        <CartDialog handleClose={handleClose} isDialogOpen={open} />
      </Suspense>
      <AppBar position="static">
        <Toolbar>
          <Typography className={classes.title} variant="h6" noWrap>
            <Link to={"/"}>
              <img
                src={require("../../assetes/logo.png")}
                height="75"
                width="75"
                alt="emart shopping"
              />
            </Link>
          </Typography>
          <div className={classes.search}>
            <SearchBar />
          </div>
          {loadRightMenu}
        </Toolbar>
      </AppBar>
      {renderMenu}
    </div>
  );
}

export default Header;
