import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Skeleton from "@material-ui/lab/Skeleton";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    height: "calc(100% - 0px)",
    justifyContent: "space-between",
    display: "flex",
    flexDirection: "column"
  }
});

// Container components handle all state changes. They are interested in data and how it changes
export default function CardSkeleton() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Skeleton animation="wave" variant="rect" width={295} height={160} />
      <Skeleton animation="wave"  variant="text"/>
      <Skeleton animation="wave" variant="text" />
      <Skeleton animation="wave"  variant="rect" width="50%" height={50}/>
    </React.Fragment>
  );
}
