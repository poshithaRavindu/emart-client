import { constant } from "../constant";
import axios from "axios";
import Cookies from "js-cookie";

axios.defaults.baseURL = constant.API_BASE_URL;
axios.defaults.headers.common["Authorization"] = localStorage.getItem(
  "AUTH_TOKEN"
);
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";

axios.defaults.headers.common["x-jwt-token"] = Cookies.get("token");

export async function get(path) {
  try {
    const { data } = await axios.get(path);
    return data;
  } catch (error) {
    const { data } = error.response;
    return data;
  }
}

export async function post(path, model) {
  try {
    const response = await axios.post(path, model);
    console.log(response);
    return response;
  } catch (error) {
    const { data } = error.response;
    return data;
  }
}

export async function put(path, model) {
  try {
    const { data } = await axios.put(path, model);
    return data;
  } catch (error) {
    const { data } = error.response;
    return data;
  }
}

export async function del(path) {
  try {
    const response = await axios.delete(path);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function upload(path, file) {
  try {
    const response = await axios.post(path, file, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });
    return response;
  } catch (error) {
    console.error(error);
  }
}
