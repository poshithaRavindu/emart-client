export const getFormattedPriceRange = value => {
  if (value) {
    try {
      const currency = JSON.parse(
        sessionStorage.getItem("currencyDetails")
          ? sessionStorage.getItem("currencyDetails")
          : { currencyRate: "1", currencyCode: "LKR" }
      );

      return new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: currency.currencyCode
      }).format(currency.currencyRate * value);
    } catch (e) {
      if (e instanceof SyntaxError) {
        console.log(e);
      } else {
        console.log(e);
      }
    }
  }
  return null;
};
