import React from 'react'
import { Redirect } from 'react-router-dom'
import Cookies from "js-cookie";

export default class ProtectedRoute extends React.Component {

    render() {
        const Component = this.props.component;
        const isAuthenticated = Cookies.get("userType") && Cookies.get("userType") === 'Admin' ? true : false

        return isAuthenticated ? (
            <Component />
        ) : (
                <Redirect to={{ pathname: '/404' }} />
            );
    }
}

