import React, { useState } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import productContext from "./services/productContext";
import cartContext from "./services/cartContext";
import "./App.css";
import Routes from "./Routes";
import Header from "./components/Header/Header";
// https://material-ui.com/components/app-bar/#app-bar-with-a-primary-search-field
function App() {
  const product = useState({});
  const cartProductCount = useState(0);
  return (
    <React.Fragment>
      <CssBaseline />
      <productContext.Provider value={product}>
        <cartContext.Provider value={cartProductCount}>
          <Header />
          <Routes />
        </cartContext.Provider>
      </productContext.Provider>
    </React.Fragment>
  );
}

export default App;
