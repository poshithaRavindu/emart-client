import {
  get,
  post,
  upload,
  put,
  del
} from "../../../services/base-api-service";

export async function AddProductService(model) {
  try {
    const route = "/products/";
    const response = await post(route, model);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function updateProductService(model) {
  try {
    const route = "/products/";
    const response = await put(route, model);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function UploadImage(model) {
  try {
    const route = "/products/upload";
    const response = await upload(route, model);
    console.log(response.data);
    return response.data;
  } catch (error) {
    console.error(error);
  }
}

export async function GetProductsService() {
  try {
    const route = "/products/";
    const response = await get(route);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function GetAllPaymentsService() {
  try {
    const route = "/orders/";
    const response = await get(route);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function deleteProductService(id) {
  try {
    const route = "/products/" + id;
    const response = await del(route);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function GetUsersService() {
  try {
    const route = "/users/";
    const response = await get(route);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function UpdateUsersService(model) {
  try {
    const route = "/users/";
    const response = await put(route, model);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function UpdateCurrencyDataService() {
  try {
    const route = "/currency";
    const response = await get(route);
    return response;
  } catch (error) {
    console.error(error);
  }
}
