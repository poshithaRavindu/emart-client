import React, { useState, Suspense } from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import IconButton from "@material-ui/core/IconButton";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { GetProductsService } from "../services/admin-service";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
const ProductDialog = React.lazy(() => import("../components/ProductDialog"));

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: "#3f51b5",
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14,
    width: "200px"
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700
  }
});

export default function ViewProducts() {
  const classes = useStyles();
  const [products, setproducts] = useState([]);

  const [open, setOpen] = React.useState(false);
  const [action, setAction] = React.useState("");

  const [product, setproduct] = useState({});

  const handleClickOpen = (event, object) => {
    setOpen(true);
    setAction(event);
    setproduct(object);
  };

  const handleClose = () => {
    setOpen(false);
    getAllProducts();
  };
  React.useEffect(() => {
    getAllProducts();
  }, []);

  const getAllProducts = async () => {
    const { products } = (await GetProductsService()) || {};
    console.log(products);
    setproducts(products);
  };

  return (
    <Container maxWidth="xl">
      <Typography variant="h4" color="textPrimary" align="left">
        Products
      </Typography>
      {typeof products != "undefined" ? (
        <React.Fragment>
          <br />
          <Button
            onClick={() => handleClickOpen("Add", {})}
            variant="outlined"
            color="#3f51b5"
            style={{ backgroundColor: "#3f51b5" }}
          >
            <b>Add Product</b>
          </Button>
          <Suspense fallback={<CircularProgress />}>
            <ProductDialog
              handleClose={handleClose}
              isDialogOpen={open}
              action={action}
              {...product}
            />
          </Suspense>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell align="right">Title</StyledTableCell>
                  <StyledTableCell align="right">Price (Rs.)</StyledTableCell>
                  <StyledTableCell align="right">Description</StyledTableCell>
                  <StyledTableCell align="right">Action</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {products.map(row => (
                  <StyledTableRow key={row._id}>
                    <StyledTableCell align="right">{row.title}</StyledTableCell>
                    <StyledTableCell size="small" align="right">
                      {row.price}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      {row.description}
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      <IconButton
                        aria-label="delete"
                        color="primary"
                        onClick={() => handleClickOpen("Edit", row)}
                      >
                        <EditIcon fontSize="small" />
                      </IconButton>
                      <IconButton
                        aria-label="delete"
                        color="primary"
                        onClick={() => handleClickOpen("Delete", row)}
                      >
                        <DeleteIcon fontSize="small" />
                      </IconButton>
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </React.Fragment>
      ) : (
        <h3>Service Temporally Unavailable</h3>
      )}
    </Container>
  );
}
