import React from "react";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import NavCard from "../components/NavCard";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  }
}));

export default function AdminPanel() {
  const classes = useStyles();

  const nav = [
    { title: "Product", imgName: "boxes.png", nav: "/admin/products" },
    { title: "Users", imgName: "user.png", nav: "/admin/users" },
    { title: "Admin Settings", imgName: "setting.png", nav: "/admin/settings" }
  ];

  return (
    <Container maxWidth="lg">
      <div className={classes.root}>
        <br />
        <br />
        <Grid container spacing={3}>
          {nav.map((navLink, index) => {
            return (
              <Grid item xs={6} sm={3} key={index}>
                <NavCard {...navLink} />
              </Grid>
            );
          })}
        </Grid>
      </div>
    </Container>
  );
}
