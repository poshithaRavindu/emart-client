import React, { useState } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import ToggleButton from "@material-ui/lab/ToggleButton";
import { GetUsersService, UpdateUsersService } from "../services/admin-service";

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: "#3f51b5",
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14,
    width: "200px"
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700
  }
});

export default function UsersGrid() {
  const classes = useStyles();
  const [users, setUsers] = useState([]);
  const [isSending, setIsSending] = useState(false);

  const handleChange = async row => {
    row.status = !row.status;
    setIsSending(true);
    const response = await UpdateUsersService(row);
    setIsSending(false);
  };

  React.useEffect(() => {
    getAllUsers();
  }, [isSending]);

  const getAllUsers = async () => {
    const userList = await GetUsersService();
    setUsers(userList);
  };

  return (
    <Container maxWidth="xl">
      <Typography variant="h4" color="textPrimary" align="left">
        Users
      </Typography>
      <br />
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="right">First Name</StyledTableCell>
              <StyledTableCell align="right">Last Name</StyledTableCell>
              <StyledTableCell align="right">Email</StyledTableCell>
              <StyledTableCell align="right">Country</StyledTableCell>
              <StyledTableCell align="right">Status</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map(row => (
              <StyledTableRow key={row._id}>
                <StyledTableCell align="right">{row.fname}</StyledTableCell>
                <StyledTableCell align="right">{row.lname}</StyledTableCell>
                <StyledTableCell align="right">{row.email}</StyledTableCell>
                <StyledTableCell align="right">{row.country}</StyledTableCell>
                <StyledTableCell align="right">
                  <ToggleButton
                    value="check"
                    selected={row.status}
                    onChange={() => handleChange(row)}
                  >
                    {row.status ? (
                      <CloseIcon color="primary" />
                    ) : (
                      <CheckIcon color="primary" />
                    )}
                  </ToggleButton>
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
}
