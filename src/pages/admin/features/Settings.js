import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import UpdateIcon from "@material-ui/icons/Update";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { UpdateCurrencyDataService } from "../services/admin-service";
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  table: {
    Width: 200
  }
}));

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: "#3f51b5",
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14,
    width: "50px"
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

export default function Settings() {
  const classes = useStyles();
  const [Currency, setCurrency] = useState([]);
  const updateCurrency = async () => {
    const res = await UpdateCurrencyDataService();
    console.log(res);
    setCurrency(res);
    console.log(Currency);
  };
  return (
    <Container maxWidth="lg">
      <div className={classes.root}>
        <br />
        <br />
        <Grid container>
          <Button
            variant="outlined"
            style={{ backgroundColor: "#3f51b5" }}
            onClick={updateCurrency}
            startIcon={<UpdateIcon />}
          >
            Update Currency Rates
          </Button>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell align="right">Currency</StyledTableCell>
                  <StyledTableCell align="right">Rate</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {Currency.map(row => (
                  <StyledTableRow key={row._id}>
                    <StyledTableCell align="right">
                      {row.currencyCode}
                    </StyledTableCell>
                    <StyledTableCell size="small" align="right">
                      {row.currencyRate}
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </div>
    </Container>
  );
}
