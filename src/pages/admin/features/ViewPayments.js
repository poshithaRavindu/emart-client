import React, { useState, useEffect } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { GetAllPaymentsService } from "../services/admin-service";

const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
}))(TableRow);

const useStyles = makeStyles({
    table: {
        minWidth: 700,
    },
});

export default function PaymentsGrid() {
    const classes = useStyles();
    const [payments, setPaymentDetail] = useState([]);

    useEffect(() => {
        getAllPayments();
    }, []);

    const getAllPayments = async () => {
        const paymentList = await GetAllPaymentsService();
        setPaymentDetail(paymentList);
    };
    console.log(payments);
    return (
        <Container maxWidth="xl">
            <Typography variant="h4" color="textPrimary" align="left">
                Payments
            </Typography>
            <br />
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell align="center">ID</StyledTableCell>
                            <StyledTableCell align="center">Product Count</StyledTableCell>
                            <StyledTableCell align="left">Customer</StyledTableCell>
                            <StyledTableCell align="right">Total Amount (Rs.)</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {payments.map(row => (
                            <StyledTableRow key={row._id}>
                                <StyledTableCell align="center">{row.id}</StyledTableCell>
                                <StyledTableCell align="center">{row.productCount}</StyledTableCell>
                                <StyledTableCell align="left">
                                    {row.shippedTo}
                                </StyledTableCell>
                                <StyledTableCell align="right">
                                    {row.totalAmount}
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    );
}