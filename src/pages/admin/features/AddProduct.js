import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { AddProductService, UploadImage } from "../services/admin-service";
import Container from "@material-ui/core/Container";
import Alert from "@material-ui/lab/Alert";
import AlertTitle from "@material-ui/lab/AlertTitle";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

export default function AddProduct() {
  const classes = useStyles();

  const handleAddProduct = async () => {
    //get values from hook
    console.log(document.getElementById("title").value);
    console.log(document.getElementById("price").value);
    console.log(document.getElementById("description").value);
    console.log(document.getElementById("imageUrl").value);

    let newProduct = {
      title: document.getElementById("title").value,
      price: document.getElementById("price").value,
      description: document.getElementById("description").value,
      imageUrl: document.getElementById("imageUrl").value
    };
    const response = await AddProductService(newProduct);
    const { Error } = response.data;

    if (Error) {
      document.getElementById("errorAlert").innerHTML = Error;
      document.getElementById("errorAlert").style.visibility = "visible";
    }
  };

  const [selectedFile, setSelectedFile] = useState(null);
  const onChangeHandler = event => {
    console.log(event.target.files);
    setSelectedFile(event.target.files);
  };

  const onClickHandler = () => {
    const data = new FormData();
    console.log(selectedFile);
    for (var x = 0; x < selectedFile.length; x++) {
      data.append("file", selectedFile[x]);
    }
    UploadImage(data);
  };

  const handleClearInputs = () => {
    document.getElementById("errorAlert").style.visibility = "hidden";
  };

  return (
    <Container maxWidth="sm">
      <div className={classes.root}>
        <Typography
          variant="h3"
          align={"center"}
          gutterBottom
          color="textSecondary"
        >
          Add Product
        </Typography>
        <form>
          <Paper className={classes.paper}>
            <Grid container spacing={3} alignItems="center">
              <Grid xs={12} sm={12} container item spacing={2}>
                <Grid item xs={12}>
                  {/* <Paper className={classes.paper}>Image Upload</Paper> */}
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="title"
                    label="Title"
                    variant="outlined"
                    fullWidth
                    size="small"
                    // value={product.title}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="price"
                    label="Price"
                    variant="outlined"
                    fullWidth
                    size="small"
                    // value={product.price}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="description"
                    label="Discription"
                    variant="outlined"
                    fullWidth
                    size="small"
                    // value={product.description}
                  />
                </Grid>
                <Grid item xs={12}>
                  <input
                    type="file"
                    data-multiple-caption="{count} files selected"
                    multiple
                    onChange={onChangeHandler}
                  />
                  <button onClick={onClickHandler}> Upload </button>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="imageUrl"
                    label="Image URL"
                    variant="outlined"
                    fullWidth
                    size="small"
                    // value={product.imageUrl}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Button
                    variant="contained"
                    color="primary"
                    startIcon={<SaveIcon />}
                    fullWidth
                    onClick={handleAddProduct}
                  >
                    Save
                  </Button>
                </Grid>
                <Grid item xs={6}>
                  <Button
                    variant="contained"
                    color="secondary"
                    startIcon={<DeleteIcon />}
                    fullWidth
                    onClick={handleClearInputs}
                  >
                    Clear
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </form>
      </div>
      {/* <Alert
        id="successAlert"
        severity="success"
        style={{ visibility: "hidden" }}
      >
        <AlertTitle>Success</AlertTitle>
        Product is Added !
      </Alert> */}
      <Alert severity="error" id="errorAlert" style={{ visibility: "hidden" }}>
        <AlertTitle>Error</AlertTitle>
      </Alert>
    </Container>
  );
}
