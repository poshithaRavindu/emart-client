import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import SaveIcon from "@material-ui/icons/Save";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Alert from "@material-ui/lab/Alert";
import AlertTitle from "@material-ui/lab/AlertTitle";
import {
  AddProductService,
  UploadImage,
  updateProductService,
  deleteProductService
} from "../services/admin-service";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

export default function ProductDialog(props) {
  const classes = useStyles();

  const handleProductAction = async () => {
    if (props.action === "Add") {
      let newProduct = {
        title: document.getElementById("title").value,
        price: document.getElementById("price").value,
        description: document.getElementById("description").value,
        imageUrl: document.getElementById("imageUrl").value,
        fullDescription: document.getElementById("fullDescription").value
      };
      const { code, data, error } = await AddProductService(newProduct);
      if (!error) {
        props.handleClose();
      } else {
        document.getElementById("errorAlert").innerHTML = error.message;
        document.getElementById("errorAlert").style.visibility = "visible";
      }
    } else if (props.action === "Edit") {
      let newProduct = {
        _id: props._id,
        title: document.getElementById("title").value,
        price: document.getElementById("price").value,
        description: document.getElementById("description").value,
        imageUrl: document.getElementById("imageUrl").value,
        fullDescription: document.getElementById("fullDescription").value
      };
      const { code, data, error } = await updateProductService(newProduct);
      if (!error) {
        props.handleClose();
      } else {
        document.getElementById("errorAlert").innerHTML = error.message;
        document.getElementById("errorAlert").style.visibility = "visible";
      }
    } else if (props.action === "Delete") {
      const _id = props._id;
      const { code, data, error } = await deleteProductService(_id);
      if (!error) {
        props.handleClose();
      } else {
        document.getElementById("errorAlert").innerHTML = error.message;
        document.getElementById("errorAlert").style.visibility = "visible";
      }
    }
  };

  const [selectedFile, setSelectedFile] = useState(null);
  const onChangeHandler = event => {
    console.log(event.target.files);
    setSelectedFile(event.target.files);
  };

  const onClickHandler = async event => {
    event.preventDefault();
    const data = new FormData();
    console.log(selectedFile);
    data.append("file", selectedFile[0]);
    const { fileurl } = await UploadImage(data);
    document.getElementById("imageUrl").value = fileurl;
  };

  return (
    <Dialog
      open={props.isDialogOpen}
      onClose={() => props.handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogContent>
        <Container maxWidth="sm">
          <div className={classes.root}>
            <Typography
              variant="h3"
              align={"center"}
              gutterBottom
              color="textSecondary"
            >
              {props.action} Product
            </Typography>
            {props.action === "Delete" ? (
              <p>
                Do you want to delete <b>{props.title}</b>
              </p>
            ) : (
                <form>
                  <Paper className={classes.paper}>
                    <Grid container spacing={3} alignItems="center">
                      <Grid xs={12} sm={12} container item spacing={2}>
                        <Grid item xs={12}>
                          {/* <Paper className={classes.paper}>Image Upload</Paper> */}
                        </Grid>
                        <Grid item xs={12}>
                          <TextField
                            id="title"
                            label="Title"
                            variant="outlined"
                            fullWidth
                            size="small"
                            defaultValue={props.title}
                          />
                        </Grid>
                        <Grid item xs={12}>
                          <TextField
                            id="price"
                            label="Price"
                            variant="outlined"
                            fullWidth
                            size="small"
                            defaultValue={props.price}
                          />
                        </Grid>
                        <Grid item xs={12}>
                          <TextField
                            id="description"
                            label="Discription"
                            variant="outlined"
                            fullWidth
                            size="small"
                            defaultValue={props.description}
                          />
                        </Grid>
                        <Grid item xs={12}>
                          <input type="file" onChange={onChangeHandler} />
                          <button onClick={onClickHandler}> Upload </button>
                        </Grid>
                        <Grid item xs={12}>
                          <TextField
                            id="imageUrl"
                            label="Image URL"
                            variant="outlined"
                            fullWidth
                            size="small"
                            defaultValue={props.imageUrl}
                          />
                        </Grid>
                        <Grid item xs={12}>
                          <TextField
                            id="fullDescription"
                            label="Full Description"
                            multiline
                            rows="4"
                            variant="outlined"
                            fullWidth
                            size="small"
                            defaultValue={props.fullDescription}
                          />
                        </Grid>
                      </Grid>
                    </Grid>
                  </Paper>
                </form>
              )}
          </div>
          <Alert
            severity="error"
            id="errorAlert"
            style={{ visibility: "hidden" }}
          >
            <AlertTitle>Error</AlertTitle>
          </Alert>
        </Container>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          color="primary"
          startIcon={<SaveIcon />}
          fullWidth
          onClick={handleProductAction}
        >
          {props.action} & Save
        </Button>
        <Button
          variant="contained"
          color="secondary"
          startIcon={<CloseIcon />}
          fullWidth
          onClick={() => props.handleClose()}
        >
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}
