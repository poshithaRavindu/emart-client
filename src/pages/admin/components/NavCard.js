import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";

export default function NavCard(props) {
  return (
    <Link to={props.nav} style={{ textDecoration: "none" }}>
      <Card style={{ width: 275, height: 275 }}>
        <CardContent>
          <Typography variant="h5" component="h2">
            {props.title}
          </Typography>
          <img
            src={require(`../../../assetes/${props.imgName}`)}
            alt={props.title}
          />
        </CardContent>
      </Card>
    </Link>
  );
}
