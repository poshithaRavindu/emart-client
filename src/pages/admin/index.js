import React from "react";
import AdminPanel from "./features/AdminPanel";

export default function Admin() {
  return (
    <div>
      <AdminPanel />
    </div>
  );
}
