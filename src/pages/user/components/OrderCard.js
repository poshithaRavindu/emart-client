import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { getFormattedPriceRange } from "../../../currnecyHandler";

export function OrderCard(props) {
  return (
    <Card>
      <CardContent>
        <Typography gutterBottom variant="h6" component="h6">
          Owner : {props.order.shipping.fname} {props.order.shipping.lname}
        </Typography>
        <Typography gutterBottom variant="body2" component="p">
          Shiping to : {props.order.shipping.addrLine1}{" "}
          {props.order.shipping.addrLine2}, {props.order.shipping.country},{" "}
          {props.order.shipping.city}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          Amount :{" "}
          {getFormattedPriceRange(props.order.payment.total)
            ? getFormattedPriceRange(props.order.payment.total)
            : `Rs.${props.order.payment.total}.00`}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default OrderCard;
