import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Grid from "@material-ui/core/Grid";
import { getFormattedPriceRange } from "../../../currnecyHandler";
let payments = [
  { name: "Card type", detail: "Visa" },
  { name: "Card holder", detail: "" },
  { name: "Card number", detail: "" },
  { name: "Expiry date", detail: "" }
];
let addresses = ["", ""];

const useStyles = makeStyles(theme => ({
  listItem: {
    padding: theme.spacing(1, 0)
  },
  total: {
    fontWeight: 700
  },
  title: {
    marginTop: theme.spacing(2)
  }
}));
let shipping;
let payment;
export default function Review(props) {
  const [products, setProducts] = useState([]);
  const [total, setTotal] = useState(0);

  const classes = useStyles();

  useEffect(() => {
    let tempArr = [];
    let tempTotal = 0;

    Object.entries(props).forEach(([key, p]) => {
      tempArr.push({
        name: p.productId.title,
        desc: p.productId.description,
        price: p.quantity * p.productId.price,
        quantity: p.quantity
      });
      tempTotal += p.productId.price * p.quantity;
    });

    setProducts(tempArr);
    setTotal(tempTotal);

    shipping = JSON.parse(sessionStorage.getItem("shippingDetail"));
    payment = JSON.parse(sessionStorage.getItem("cardDetails"));
    payments = [
      { name: "Card type", detail: "Visa" },
      { name: "Card holder", detail: payment?.name },
      { name: "Card number", detail: payment?.cardNo },
      { name: "Expiry date", detail: payment?.expireDate }
    ];
    addresses = [
      shipping?.addrLine1 + "," + shipping?.addrLine2,
      shipping?.city,
      shipping?.state,
      shipping?.zip,
      shipping?.country
    ];
  }, []);
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Order summary
      </Typography>
      <List disablePadding>
        {products.map(product => (
          <ListItem className={classes.listItem} key={product.name}>
            <ListItemText primary={product.name} secondary={product.desc} />
            <Typography variant="body2">
              {getFormattedPriceRange(product.price)
                ? getFormattedPriceRange(product.price)
                : `Rs.${product.price}.00`}
            </Typography>
          </ListItem>
        ))}
        <ListItem className={classes.listItem}>
          <ListItemText primary="Total" />
          <Typography variant="subtitle1" className={classes.total}>
            {getFormattedPriceRange(total)
              ? getFormattedPriceRange(total)
              : `Rs.${total}.00`}
          </Typography>
        </ListItem>
      </List>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <Typography variant="h6" gutterBottom className={classes.title}>
            Shipping
          </Typography>
          <Typography gutterBottom>
            {shipping?.fname + " " + shipping?.lname}
          </Typography>
          <Typography gutterBottom>{addresses.join(", ")}</Typography>
        </Grid>
        <Grid item container direction="column" xs={12} sm={6}>
          <Typography variant="h6" gutterBottom className={classes.title}>
            Payment details
          </Typography>
          <Grid container>
            {payments.map(payment => (
              <React.Fragment key={payment.name}>
                <Grid item xs={6}>
                  <Typography gutterBottom>{payment.name}</Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography gutterBottom>{payment.detail}</Typography>
                </Grid>
              </React.Fragment>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
