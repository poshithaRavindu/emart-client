import React, { useState, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

let paymentDetail = {};
export default function PaymentForm() {
  const [state, setState] = useState({
    name: "",
    cardNo: "",
    expireDate: "",
    cvv: "",
    total: ""
  });

  function handleChange(evt) {
    const value = evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value
    });
    paymentDetail = state;
  }

  useEffect(() => {
    const payment = JSON.parse(sessionStorage.getItem("cardDetails"));
    console.log(payment);
    setState({
      name: payment?.name,
      cardNo: payment?.cardNo,
      expireDate: payment?.expireDate,
      cvv: payment?.cvv,
      total: payment?.total
    });
    return () => {
      console.log(paymentDetail);
      if (JSON.stringify(paymentDetail) !== "{}") {
        sessionStorage.setItem("cardDetails", JSON.stringify(paymentDetail));
        console.log("******************* UNMOUNTED");
      }
    };
  }, []);

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Payment method
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="cardName"
            name="name"
            label="Name on card"
            fullWidth
            value={state.name}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="cardNumber"
            name="cardNo"
            label="Card number"
            fullWidth
            type="number"
            value={state.cardNo}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="expDate"
            name="expireDate"
            label="Expiry date(mm/yy)"
            fullWidth
            value={state.expireDate}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="cvv"
            name="cvv"
            label="CVV"
            helperText="Last three digits on signature strip"
            fullWidth
            value={state.cvv}
            onChange={handleChange}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
