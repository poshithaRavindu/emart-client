import React, { useState, useEffect, useContext } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import CloseIcon from "@material-ui/icons/Close";
import CheckoutIcon from "@material-ui/icons/Toll";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import {
  GetUserCart,
  DeleteItemFromCart
} from "../../user/services/user-service";
import Cookies from "js-cookie";
import DeleteIcon from "@material-ui/icons/Delete";
import { Link } from "react-router-dom";
import "../styles/cart.css";
import Container from "@material-ui/core/Container";
import cartContext from "../../../services/cartContext";
import { getFormattedPriceRange } from "../../../currnecyHandler";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import IconButton from "@material-ui/core/IconButton";
import SkipPreviousIcon from "@material-ui/icons/SkipPrevious";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import SkipNextIcon from "@material-ui/icons/SkipNext";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    justifyContent: "space-between",
    display: "flex",
    flexDirection: "column"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  action: {
    textAlign: "right"
  }
}));

const useStylesCard = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  details: {
    display: "flex",
    flexDirection: "column"
  },
  content: {
    flex: "1 0 auto"
  },
  cover: {
    width: 151
  },
  controls: {
    display: "flex",
    alignItems: "center",
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  playIcon: {
    height: 38,
    width: 38
  }
}));

export default function CartDialog(props) {
  const classes = useStyles();
  const classesCard = useStylesCard();
  const theme = useTheme();
  const [cartProductCount, setcartProductCount] = useContext(cartContext);
  const [cart, setCart] = useState([]);

  useEffect(() => {
    getUserCartDetails();
  }, [props.isDialogOpen]);

  const getUserCartDetails = async () => {
    const cartItems = await GetUserCart(Cookies.get("userID"));
    setCart(cartItems);
    setcartProductCount(cartItems.length || 0);
  };

  const [selectedFile, setSelectedFile] = useState(null);
  const onChangeHandler = event => {
    console.log(event.target.files);
    setSelectedFile(event.target.files);
  };

  const deleteCartItemHandler = async productId => {
    await DeleteItemFromCart(Cookies.get("userID"), productId);
    await getUserCartDetails();
  };

  return (
    <Dialog
      open={props.isDialogOpen}
      onClose={() => props.handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogContent>
        <Container maxWidth="sm">
          <div className={classes.root}>
            {typeof cart != "undefined" ? (
              <React.Fragment>
                <Typography
                  variant="h3"
                  align={"center"}
                  gutterBottom
                  color="textSecondary"
                >
                  Cart Items
                </Typography>
                <ul className="cart__item-list">
                  {cart.length ? (
                    cart.map((p, index) => {
                      return (
                        <Grid item xs={12} style={{ padding: "10px" }}>
                          <Card className={classesCard.root}>
                            <CardMedia
                              className={classesCard.cover}
                              image={p.productId.imageUrl}
                              title="Product Image"
                            />
                            <div className={classesCard.details}>
                              <CardContent className={classesCard.content}>
                                <Typography component="h5" variant="h5">
                                  {p.productId.title}
                                </Typography>
                                <Typography
                                  variant="subtitle1"
                                  color="textSecondary"
                                >
                                  Quantity : {p.quantity}
                                </Typography>
                                <Typography
                                  variant="subtitle1"
                                  color="textSecondary"
                                >
                                  Total :{" "}
                                  {getFormattedPriceRange(
                                    p.quantity * p.productId.price
                                  )
                                    ? getFormattedPriceRange(
                                        p.quantity * p.productId.price
                                      )
                                    : `Rs.${p.quantity * p.productId.price}.00`}
                                </Typography>
                              </CardContent>
                              <div className={classesCard.controls}>
                                <Button
                                  variant="contained"
                                  color="secondary"
                                  startIcon={<DeleteIcon />}
                                  onClick={() =>
                                    deleteCartItemHandler(p.productId._id)
                                  }
                                >
                                  Remove
                                </Button>
                              </div>
                            </div>
                          </Card>
                        </Grid>
                      );
                    })
                  ) : (
                    <h2> Your cart is empty ...Lets do shopping!</h2>
                  )}
                </ul>
              </React.Fragment>
            ) : (
              <h1>service unavailable</h1>
            )}
          </div>
        </Container>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          color="secondary"
          startIcon={<CloseIcon />}
          fullWidth
          onClick={() => props.handleClose()}
        >
          Close
        </Button>
        <Button
          component={Link}
          to={{
            pathname: "/checkout",
            state: { cart: cart }
          }}
          variant="contained"
          color="primary"
          startIcon={<CheckoutIcon />}
          fullWidth
          onClick={() => props.handleClose()}
        >
          Check Out
        </Button>
      </DialogActions>
    </Dialog>
  );
}
