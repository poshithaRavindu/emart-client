import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";


let shipAddr = {};
export default function AddressForm() {
  const [state, setState] = useState({
    fname: "",
    lname: "",
    addrLine1: "",
    addrLine2: "",
    country: "",
    city: "",
    state: "",
    zip: "",
    shipping: ""
  });
  function handleChange(evt) {
    const value = evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value
    });
    shipAddr = state;
  }

  useEffect(() => {
    const shipping = JSON.parse(sessionStorage.getItem("shippingDetail"));
    console.log(shipping);
    setState({
      fname: shipping?.fname,
      lname: shipping?.lname,
      addrLine1: shipping?.addrLine1,
      addrLine2: shipping?.addrLine2,
      country: shipping?.country,
      city: shipping?.city,
      state: shipping?.state,
      zip: shipping?.zip,
      shipping: shipping?.shipping
    });
    return () => {
      if (JSON.stringify(shipAddr) !== "{}") {
        console.log(shipAddr);
        sessionStorage.setItem("shippingDetail", JSON.stringify(shipAddr));
        console.log("******************* UNMOUNTED");
      }
    };
  }, []);
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Shipping address
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="firstName"
            name="fname"
            label="First name"
            fullWidth
            autoComplete="fname"
            value={state.fname}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="lastName"
            name="lname"
            label="Last name"
            fullWidth
            autoComplete="lname"
            value={state.lname}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="address1"
            name="addrLine1"
            label="Address line 1"
            fullWidth
            autoComplete="billing address-line1"
            value={state.addrLine1}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="address2"
            name="addrLine2"
            label="Address line 2"
            fullWidth
            autoComplete="billing address-line2"
            value={state.addrLine2}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="city"
            name="city"
            label="City"
            fullWidth
            autoComplete="billing address-level2"
            value={state.city}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            id="state"
            name="state"
            label="State/Province/Region"
            fullWidth
            value={state.state}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="zip"
            name="zip"
            label="Zip / Postal code"
            fullWidth
            autoComplete="billing postal-code"
            value={state.zip}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          {/* <TextField
            required
            id="country"
            name="country"
            label="Country"
            fullWidth
            autoComplete="billing country"
            value={state.country}
            onChange={handleChange}
          /> */}
          <div style={{ height: "16px" }}></div>
          <Select
            native
            label="Country"
            id="country"
            required
            name="country"
            fullWidth
            value={state.country}
            onChange={handleChange}
          >
          </Select>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
