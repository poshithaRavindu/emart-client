import React, { useEffect, useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import {
  GetUserDetails,
  UpdateUserProfileService
} from "../services/user-service";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import Cookies from "js-cookie";
import { countries } from "../../../assetes/countries";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export default function ViewUserProfile() {
  const classes = useStyles();

  const [user, setUser] = useState({});

  const [errorMessage, setErrorMessage] = useState();
  const [open, setOpen] = React.useState(false);
  const [message, setMessage] = React.useState(null);
  const [type, setType] = React.useState("error");

  const handleClick = (message, type) => {
    setMessage(message);
    setOpen(true);
    setType(type);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  useEffect(async () => {
    await getUserProfile();
  }, []);

  const getUserProfile = async () => {
    const { code, data, error } = await GetUserDetails(Cookies.get("userID"));
    if (!error) {
      setUser(data.user);
      console.log(user);
    } else {
      setErrorMessage(error.message);
    }
  };

  const clearFields = () => {

    document.getElementById("password").value = "";
    document.getElementById("newPassword").value = "";
    document.getElementById("confirmPassword").value = "";
  };

  const handleChanges = async () => {
    let currentPassword = document.getElementById("password").value;
    let newPassword = document.getElementById("newPassword").value;
    let confirmPassword = document.getElementById("confirmPassword").value;
    let updateUser = {};

    if (currentPassword != '') {
      if (newPassword != '') {
        if (newPassword === confirmPassword) {
          updateUser = {
            _id: Cookies.get("userID"),
            fname: document.getElementById("firstName").value,
            lname: document.getElementById("lastName").value,
            email: document.getElementById("email").value,
            country: document.getElementById("country").value,
            password: document.getElementById("newPassword").value,
            oldPassword: currentPassword
          };
        }
      }
    } else {
      updateUser = {
        _id: Cookies.get("userID"),
        fname: document.getElementById("firstName").value,
        lname: document.getElementById("lastName").value,
        email: document.getElementById("email").value,
        country: document.getElementById("country").value
      };
    }

    const { code, data, error } = await UpdateUserProfileService(
      Cookies.get("userID"),
      updateUser
    );

    if (!error) {
      handleClick("Profile Updated Successfully", "success");
    } else {
      handleClick(error.message, "error");
    }

    clearFields();
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        {errorMessage ? (
          <h3>No Product Found</h3>
        ) : (
          <React.Fragment>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              User Profile
            </Typography>
            <form className={classes.form} noValidate>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="firstName"
                    label="First Name"
                    defaultValue={user.fname}
                    key={user.fname}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="lastName"
                    label="Last Name"
                    defaultValue={user.lname}
                    key={user.lname}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    defaultValue={user.email}
                    key={user.email}
                    disabled
                  />
                </Grid>
                <Grid item xs={12}>
                  <InputLabel htmlFor="country">Country</InputLabel>
                  <Select
                    native
                    variant="outlined"
                    required
                    fullWidth
                    id="country"
                    label="Country"
                    defaultValue={user.country}
                    key={user.country}
                  >
                    {countries.map(({ name, code }, index) => (
                      <option
                        key={index}
                        value={code}
                        selected={user.country === code ? true : false}
                      >
                        {name}
                      </option>
                    ))}
                    <option>{user.country}</option>
                  </Select>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    label="Current Password"
                    id="password"
                    type="password"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    label="New Password"
                    id="newPassword"
                    type="password"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="confirmPassword"
                    label="Confirm Password"
                    variant="outlined"
                    fullWidth
                    type="password"
                  />
                </Grid>
              </Grid>
              <Button
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={handleChanges}
              >
                Save Changes
              </Button>
            </form>{" "}
          </React.Fragment>
        )}
      </div>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={type}>
          {message}
        </Alert>
      </Snackbar>
    </Container>
  );
}
