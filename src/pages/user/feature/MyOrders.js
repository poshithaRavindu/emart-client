import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import OrderCard from "../components/OrderCard";
import { getMyOrdersService } from "../services/user-service";
import CircularProgress from "@material-ui/core/CircularProgress";
import Cookies from "js-cookie";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

function MyOrders() {
  const classes = useStyles();

  const [orders, setOrders] = useState([]);

  useEffect(() => {
    getMyOrders(Cookies.get("userID"));
  }, []);

  const getMyOrders = async userId => {
    const orders = await getMyOrdersService(userId);
    setOrders(orders);
  };

  return (
    <Container maxWidth="lg" style={{ padding: "20px" }}>
      <div className={classes.root}>
        <Grid container spacing={3}>
          {typeof orders === "undefined" ? (
            <h1>No order found</h1>
          ) : orders.length ? (
            orders.map((order, index) => {
              return (
                <Grid item xs={12} sm={12} key={index}>
                  <OrderCard order={order} />
                </Grid>
              );
            })
          ) : (
            <h2> Hmmmm... No orders were found!</h2>
          )}
        </Grid>
      </div>
    </Container>
  );
}

export default MyOrders;
