import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import AddressForm from "../components/AddressForm";
import PaymentForm from "../components/PaymentForm";
import Review from "../components/Review";
import Cookies from "js-cookie";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Link } from "react-router-dom";
import StoreIcon from "@material-ui/icons/Store";
import { countries } from "../../../assetes/countries";
import Alert from "@material-ui/lab/Alert";
import AlertTitle from "@material-ui/lab/AlertTitle";

import {
  CreateOrder,
  GetUserCart,
  GetUserDetails
} from "../services/user-service";

const useStyles = makeStyles(theme => ({
  appBar: {
    position: "relative"
  },
  layout: {
    width: "auto",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3)
    }
  },
  stepper: {
    padding: theme.spacing(3, 0, 5)
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end"
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1)
  }
}));

const steps = ["Shipping address", "Payment details", "Review your order"];

let cartItems = [];

function getStepContent(step) {
  switch (step) {
    case 0:
      return <AddressForm />;
    case 1:
      return <PaymentForm />;
    case 2:
      return <Review {...cartItems} />;
    default:
      throw new Error("Unknown step");
  }
}

export default function Checkout(props) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [message, setMessage] = React.useState(null);
  const [type, setType] = React.useState("error");
  const [open, setOpen] = React.useState(false);

  const OpenSuccessDialog = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleNext = () => {
    const shipping = JSON.parse(sessionStorage.getItem("shippingDetail"));
    const payment = JSON.parse(sessionStorage.getItem("cardDetails"));

    // console.log(shipping,"Hello", shipping.fname );
    // if (activeStep == 0) {
    //   if (shipping.fname !== "" && shipping.lname !== "" && shipping.addrLine1 !== "" && shipping.country !== "" 
    //   && shipping.city !== "" && shipping.state !== "" && shipping.state !== "" && shipping.zip !== "") {
    //     setActiveStep(activeStep + 1);
    //   }
    //   else {
    //     document.getElementById("errorAlert").innerHTML = "Please Fill All Valuse";
    //     document.getElementById("errorAlert").style.visibility = "visible";
    //   }
    // }
    // else if (activeStep == 1) {
    //   if (payment.name !== "" && payment.cardNo !== "" && payment.expireDate !== "" 
    //   && payment.cvv !== "" && payment.total !== "") {
    //     setActiveStep(activeStep + 1);
    //   }
    //   else {
    //     document.getElementById("errorAlert").innerHTML = "Please Fill All Valuse";
    //     document.getElementById("errorAlert").style.visibility = "visible";
    //   }
    // }
    setActiveStep(activeStep + 1);

  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  const createOrder = async () => {
    const shipping = JSON.parse(sessionStorage.getItem("shippingDetail"));
    const payment = JSON.parse(sessionStorage.getItem("cardDetails"));
    const user = await GetUserDetails(Cookies.get("userID"));
    //const cartItems = await GetUserCart(Cookies.get("userID"));
    console.log("order");
    const order = {
      products: props.location.state.cart,
      user: {
        userId: Cookies.get("userID")
      },
      shipping: {
        fname: shipping.fname,
        lname: shipping.lname,
        addrLine1: shipping.addrLine1,
        addrLine2: shipping.addrLine2,
        country: shipping.country,
        city: shipping.city,
        state: shipping.state,
        zip: shipping.zip,
        shipping: "Free"
      },
      payment: {
        name: payment.name,
        cardNo: payment.cardNo,
        expireDate: payment.expireDate,
        cvv: payment.cvv,
        total: payment.total
      }
    };

    const response = await CreateOrder(order);
    console.log(response);

    OpenSuccessDialog();
  };

  const setCountries = async () => {
    var country = document.getElementById("country");
    for (var i = 0; i < countries.length; i++) {
      country.innerHTML =
        country.innerHTML +
        '<option value="' +
        countries[i]["code"] +
        '">' +
        countries[i]["name"] +
        "</option>";
    }
  };

  useEffect(() => {
    // console.log(props.location.state.cart);
    //Access the cart obejct from here
    cartItems = props.location.state.cart;
    setCountries();
  }, []);

  return (
    <React.Fragment>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        disableBackdropClick="true"
      >
        <DialogTitle id="alert-dialog-title">{"Success"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            You Successfully Placed Your Order
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleClose}
            color="primary"
            autoFocus
            component={Link}
            to={{
              pathname: "/"
            }}
            variant="contained"
            startIcon={<StoreIcon />}
          >
            Go to Store
          </Button>
        </DialogActions>
      </Dialog>
      <CssBaseline />
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography component="h1" variant="h4" align="center">
            Checkout
          </Typography>
          <Stepper activeStep={activeStep} className={classes.stepper}>
            {steps.map(label => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <React.Fragment>
            {activeStep === steps.length ? (
              <React.Fragment>
                <Typography variant="h5" gutterBottom>
                  Thank you for your order.
                </Typography>
                <Typography variant="subtitle1">
                  Your order number is #2001539. We have emailed your order
                  confirmation, and will send you an update when your order has
                  shipped.
                </Typography>
              </React.Fragment>
            ) : (
              <React.Fragment>
                {getStepContent(activeStep)}
                <div className={classes.buttons}>
                  {activeStep !== 0 && (
                    <Button onClick={handleBack} className={classes.button}>
                      Back
                    </Button>
                  )}
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={
                      activeStep === steps.length - 1 ? createOrder : handleNext
                    }
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? "Place order" : "Next"}
                  </Button>
                </div>
                <Alert
                  severity="error"
                  id="errorAlert"
                  style={{ visibility: "hidden" }}
                >
                  <AlertTitle>Error</AlertTitle>
                </Alert>
              </React.Fragment>
            )}
          </React.Fragment>
        </Paper>
      </main>
    </React.Fragment>
  );
}
