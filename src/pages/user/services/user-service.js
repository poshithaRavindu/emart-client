import { get, post, put, del } from "../../../services/base-api-service";

export async function UserLoginService(model) {
  try {
    const route = "/auth/";
    const response = await post(route, model);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function UserRegistrationService(model) {
  try {
    const route = "/users/";
    const response = await post(route, model);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function GetUserCart(userId) {
  try {
    const route = "/cart/" + userId;
    const response = await get(route);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function AddProductToCart(model) {
  try {
    const route = "/cart/";
    const response = await post(route, model);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function UpdateUserProfileService(userId, model) {
  try {
    const route = "/users/" + userId;
    const response = await put(route, model);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function GetUserDetails(userId) {
  try {
    const route = "/users/" + userId;
    const { code, data, error } = await get(route);
    return { code, data, error };
  } catch (error) {
    console.error(error);
  }
}

export async function DeleteItemFromCart(userId, productId) {
  try {
    const route = "/cart/" + userId + "/" + productId;
    const response = await del(route);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function getMyOrdersService(userId) {
  try {
    const route = "/orders/my-orders/" + userId;
    const response = await get(route);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function CreateOrder(model) {
  try {
    const route = "/orders/";
    const response = await post(route, model);
    return response;
  } catch (error) {
    console.error(error);
  }
}
