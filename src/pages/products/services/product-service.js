import { get } from "../../../services/base-api-service";

export async function GetProductsService() {
  try {
    const route = "/products/";
    const response = await get(route);
    console.log(response);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function GetProductDetailsById(productId) {
  try {
    const route = "/products/" + productId;
    const { code, data, error } = await get(route);
    return { code, data, error };
  } catch (error) {
    return error;
  }
}
