import React, { useState, useContext } from "react";
import ProductSummery from "../../../components/ProductCard/ProductSummery";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import { useStyles } from "../styles/ViewAllProductsStyle";
import { GetProductsService } from "../services/product-service";
import productContext from "../../../services/productContext";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import CardSkeleton from "../../../components/Skeleton/CardSkeleton";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function ViewAllProducts() {
  const classes = useStyles();
  const [products, setproducts] = useContext(productContext);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState(null);
  const [type, setType] = useState("error");

  React.useEffect(() => {
    // Cookies.set("userID", "5e67b4fea1e17554445dff75");
    // Cookies.set("userName", "Suneth Shehan");
    getAllProducts();
  }, []);

  const getAllProducts = async () => {
    const productList = (await GetProductsService()) || {};
    console.log(productList);
    setproducts(productList.products);
    if (JSON.stringify(productList.meta) !== "{}") {
      sessionStorage.setItem(
        "currencyDetails",
        JSON.stringify(productList.meta)
      );
    }
  };

  const handleClick = (message, type) => {
    setMessage(message);
    setOpen(true);
    setType(type);
  };

  const skeletopDisplay =  (
      <React.Fragment>
        <Grid item xs={12} md={4} sm={2} lg={3}>
          <CardSkeleton />
        </Grid>
        <Grid item xs={12} md={4} sm={2} lg={3}>
          <CardSkeleton />
        </Grid>
        <Grid item xs={12} md={4} sm={2} lg={3}>
          <CardSkeleton />
        </Grid>
        <Grid item xs={12} md={4} sm={2} lg={3}>
          <CardSkeleton />
        </Grid>
      </React.Fragment>
    );

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const data = {
    type: "error",
    message: "test"
  };
  return (
    <Container maxWidth="lg" style={{ padding: "20px" }}>
      <div className={classes.root}>
        <Grid container spacing={3}>
          {typeof products != "undefined" ? (
            <React.Fragment>
              {products.length
                ? products.map((product, index) => {
                    return (
                      <Grid item xs={6} sm={3} key={index}>
                        <ProductSummery
                          product={product}
                          handleClick={handleClick}
                        />
                      </Grid>
                    );
                  })
                : skeletopDisplay}
            </React.Fragment>
          ) : (
            <h3>Service Temporarily Unavailable</h3>
          )}
        </Grid>
        {/* <Grid item xs={3} alignItems="center" alignContent="center"> */}
        <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
          <Alert onClose={handleClose} severity={type}>
            {message}
          </Alert>
        </Snackbar>
        {/* </Grid> */}
      </div>
    </Container>
  );
}
