import React, { useState, useEffect, useContext } from "react";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import Typography from "@material-ui/core/Typography";
import { GetProductDetailsById } from "../services/product-service";
import Cookies from "js-cookie";
import { AddProductToCart } from "../../user/services/user-service";
import { Link } from "react-router-dom";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import TextField from "@material-ui/core/TextField";
import StepLabel from "@material-ui/core/StepLabel";
import IconButton from "@material-ui/core/IconButton";
import cartContext from "../../../services/cartContext";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { getFormattedPriceRange } from "../../../currnecyHandler";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  rootCard: {
    // maxWidth: 100,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  media: {
    height: 400
  }
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function ViewProductDetails({ match }) {
  const classes = useStyles();
  const [cartProductCount, setcartProductCount] = useContext(cartContext);
  const [count, setCount] = React.useState(1);
  const [product, setProduct] = useState({});
  const [errorMessage, setErrorMessage] = useState();
  const [open, setOpen] = React.useState(false);
  const [message, setMessage] = React.useState(null);
  const [type, setType] = React.useState("error");

  const handleClick = (message, type) => {
    setMessage(message);
    setOpen(true);
    setType(type);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  useEffect(() => {
    getProductDetails();
    console.log(match);
  }, []);

  const handleAddProductoCart = async () => {
    if (!Cookies.get("userID")) {
      handleClick("Please login to add items to cart", "error");
      return;
    }

    let newItem = {
      userId: Cookies.get("userID"),
      productId: product._id,
      quantity: count
    };

    //temporilly commented
    await AddProductToCart(newItem);
    setcartProductCount(cartProductCount + 1);

    handleClick(`${product.title} is added to your cart`, "success");

    //cart items should be incremented
    // console.log(document.getElementById("cart-icon").badgeContent);
    // document.getElementById("cart-icon").badgeContent = 10
    // document.getElementById("cart-icon").setAttribute("textContent", 10);
  };

  const getProductDetails = async () => {
    const { code, data, error } = await GetProductDetailsById(match.params.id);
    if (!error) {
      setProduct(data.productDetails);
    } else {
      setErrorMessage(error.message);
    }
  };

  return (
    <Container maxWidth="lg">
      <br />
      <div className={classes.root}>
        {errorMessage ? (
          <h3>No Product Found</h3>
        ) : (
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6} md={5}>
              <Paper className={classes.paper} elevation={3}>
                <Card>
                  {/* <CardActionArea> */}
                  <CardMedia
                    className={classes.media}
                    image={product.imageUrl}
                    title="Contemplative Reptile"
                  />
                </Card>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} md={7}>
              <Typography variant="h4" color="initial" align="center">
                {product.title}
              </Typography>
              <Typography variant="h6" color="textPrimary" align="left">
                Discription
              </Typography>
              <Typography variant="body1" color="textSecondary" align="left">
                {product.description}
              </Typography>
              <br />
              <Typography variant="h5" color="secondary" component="p">
                {getFormattedPriceRange(product.price)
                  ? getFormattedPriceRange(product.price)
                  : `Rs.${product.price}.00`}
              </Typography>
              <Grid container spacing={3} alignItems="center" justify="left">
                <Grid item xs={4} sm={2}>
                  <StepLabel>
                    Quantity: <b>{count}</b>
                  </StepLabel>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <ButtonGroup>
                    <IconButton
                      color="primary"
                      aria-label="reduce"
                      onClick={() => {
                        setCount(Math.max(count - 1, 1));
                      }}
                    >
                      <RemoveIcon fontSize="normal" />
                    </IconButton>
                    <IconButton
                      color="primary"
                      aria-label="increase"
                      onClick={() => {
                        setCount(count + 1);
                      }}
                    >
                      <AddIcon fontSize="normal" />
                    </IconButton>
                  </ButtonGroup>
                </Grid>
              </Grid>
              <Grid item xs={12}>
                <br />
                <Grid item xs={6}>
                  <Button
                    variant="outlined"
                    color="primary"
                    fullWidth
                    size="large"
                    className={classes.button}
                    startIcon={<AddShoppingCartIcon />}
                    onClick={handleAddProductoCart}
                  >
                    Add to cart
                  </Button>
                </Grid>
              </Grid>
              <br />
              <Grid>
                <Typography variant="body1" color="textSecondary" align="left">
                {product.fullDescription}
                </Typography>
              </Grid>
              
            </Grid>
          </Grid>
        )}
        <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
          <Alert onClose={handleClose} severity={type}>
            {message}
          </Alert>
        </Snackbar>
      </div>
    </Container>
  );
}

export default ViewProductDetails;
