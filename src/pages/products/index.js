import React from "react";
import ViewAllProducts from "./features/ViewAllProducts";

function index() {
  return (
    <div>
      <ViewAllProducts />
    </div>
  );
}

export default index;
