import React, { useContext } from "../../../../../node_modules/react";
import SearchIcon from "../../../../../node_modules/@material-ui/icons/Search";
import { InputBase } from "../../../../../node_modules/@material-ui/core";
import "./search.css";
import { useStyles } from "../../../../components/Header/HeaderStyle";
import { SearchService } from "../../services/home-service";
import productContext from "../../../../services/productContext";

export default function SearchBar() {
  const classes = useStyles();
  const [products, setproducts] = useContext(productContext);
  const onChangeHandler = async event => {
    const value = event.target.value;
    const respons = await SearchService(value);
    setproducts(respons);
  };
  return (
    <div>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        placeholder="Search…"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput
        }}
        inputProps={{ "aria-label": "search" }}
        onChange={onChangeHandler}
      />
    </div>
  );
}
