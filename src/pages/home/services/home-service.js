import { post } from "../../../services/base-api-service";

export async function SearchService(searchText) {
  try {
    const route = "/search/";
    const response = await post(route, { searchText });
    return response.data;
  } catch (error) {
    console.error(error);
  }
}
