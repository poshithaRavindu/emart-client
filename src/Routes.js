import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./pages/home";
import ViewUserProfile from "./pages/user/feature/ViewUserProfile";
import ViewAllProducts from "./pages/products";
import Admin from "./pages/admin/index";
import NotFound from "./pages/404";
import Login from "./pages/user/feature/Login";
import SignUp from "./pages/user/feature/Registration";
import UsersGrid from "./pages/admin/features/ViewUsers";
import PaymentsGrid from "./pages/admin/features/ViewPayments";
import Checkout from "./pages/user/feature/Checkout";
import ViewProductDetails from "./pages/products/features/ViewProductDetails";
import ViewProducts from "./pages/admin/features/ViewProducts";
import MyOrders from "./pages/user/feature/MyOrders";
import Settings from "./pages/admin/features/Settings";
import AdminRoutes from './ProtectedRoutes/AdminRoutes';
export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/user" exact component={ViewUserProfile} />
      <Route path="/product" exact component={ViewAllProducts} />
      <Route path="/product/:id" exact component={ViewProductDetails} />
      <Route path="/signup" exact component={SignUp} />
      <Route path="/payments" exact component={PaymentsGrid} />
      <Route path="/login" exact component={Login} />
      <Route path="/checkout" exact component={Checkout} />
      <Route path="/user/myorders" exact component={MyOrders} />

      <AdminRoutes path="/admin" exact component={Admin} />
      <AdminRoutes path="/admin/users" exact component={UsersGrid} />
      <AdminRoutes path="/admin/payments" exact component={PaymentsGrid} />
      <AdminRoutes path="/admin/settings" exact component={Settings} />
      <AdminRoutes path="/admin/products" exact component={ViewProducts} />

      <Route path="/404" component={NotFound} />

      <Route component={NotFound} />

    </Switch>
  );
}
